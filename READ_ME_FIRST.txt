   File: READ_ME_FIRST.txt

=====================================================================

   Software in R for the paper

        On the estimation of variance parameters in non-standard generalised linear mixed models: Application to penalised smoothing
        
        by MX Rodriguez-Alvarez, M Durban, D-J Lee and PHC Eilers

        Statistics and Computing, DOI: 10.1007/s11222-018-9818-2

        Contact: MX Rodriguez-Alvarez - mxrodriguez@bcamath.org

=====================================================================

There are seven files

1) and 2) SOPExamples_1.0.zip and SOPExamples_1.0.tar.gz
	R-package that contains the functions needed to fit generalised linear mixed models, spatially-adaptive P-spline models 
	and P-spline models for hierarchical curve data using the SOP (Separation of Overlapping Precision matrices) method.   

3) SOPExamples-manual.pdf
	Manual associated with the package.

4), 5) and 6) Example_adaptive_doppler.R, Example_simulation_adaptive_doppler.R, Example_adaptive_doppler.Rand Example_adaptive_x_ray_difracction.R 
	These three R-files contain the code to reproduce the analyses and simulation discussed in Section 5.1 and 5.2.
	In particular, the codes give Figures 2 and 3 of the paper.   

7) Example_Hierarchical_Curve_Data_DTI.R
	This R-files contains the code to reproduce the analyses discussed in Section 5.3. 
	In particular, the code gives Figures 4 and 5(a).   
	We note that in the paper comparisons with alternative methods are perfomed. However, for simplicity, we have only incorporated in this file the analyses using our approach.


=====================================================================
Undate: 2019-05-22: Error detected and corrected on function fit.SOP
=====================================================================
